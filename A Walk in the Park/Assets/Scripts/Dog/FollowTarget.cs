﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{

    public Transform target;
    public float speed;


    void Update()
    {
        //move towards
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed);
        //lock z position
        transform.position = new Vector3(transform.position.x, transform.position.y, -10);
    }
}
