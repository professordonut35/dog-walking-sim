﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetController : MonoBehaviour
{
    #region Declare Variables
    Transform player;
    PetPlayerInteraction petToPlayer;
    public ValueHandler mood;
    LeashController leash;
    PetAI AI;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        #region Fetch Components
        player = PlayerMovement.instance.transform;
        petToPlayer = GetComponent<PetPlayerInteraction>();
        GetComponent<LeashController>();
        AI = GetComponent<PetAI>();
        leash = GetComponent<LeashController>();
        
        #endregion

    }
    

    // Update is called once per frame
    void Update()
    {
        petToPlayer.CheckPlayerInteractions(player, mood);
        AI.UpdatePet(player, mood, leash.GetLength());
        leash.LeashConstrain(player);
        leash.RenderLeash(player);


    }
}
