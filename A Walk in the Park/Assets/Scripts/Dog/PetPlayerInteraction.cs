﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetPlayerInteraction : MonoBehaviour
{

   

    public void CheckPlayerInteractions(Transform playerObject, ValueHandler petMood)
    {
        //if player is tugging (right click moves the player faster, but reduces the mood of the pet)
        if (playerObject.GetComponent<PlayerMovement>().tugging)
        {
            //reduce pet mood
            petMood.ValueSubtract(0.1f);
        }

        //if player is giving kibble
        if (playerObject.GetComponent<PlayerMovement>().kibble)
        {
            //top off pet mood
            petMood.ValueSet(3f);

        }
    }
}
