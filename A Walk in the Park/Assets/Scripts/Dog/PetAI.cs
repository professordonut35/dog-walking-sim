﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class PetAI : MonoBehaviour
{
    #region VarDeclaration

    [Header("References")]

    public Transform mugger;

    [Header("Speeds")]

    [SerializeField] float speed;
    [SerializeField] float dragSpeed, followSpeed, kibbleSpeed, passiveSpeed, finalDragSpeed = 0;

    [Header("AI")]

    public Vector3 moveTarget;
    public float sniffRadius;
    public bool targetAcquired;
    public Vector3 unleashedTarget;
    LeashController leash;
    public enum state
    {
        passive,
        kibble,
        follow,
        drag,
        finalDrag
    }
    public state behaviorState = state.passive;



    [Header("Misc")]

    //stuck locks the pet into its state after reaching the defined finish.
    bool stuck = false;
    public LayerMask lure;

    #endregion

    FMOD.Studio.EventInstance whimper;

    //START
    void Start()
    {
        leash = this.gameObject.GetComponent<LeashController>();
        targetAcquired = false;

        whimper = RuntimeManager.CreateInstance("event:/Voice/Dog Whimper");
    }

    public void UpdatePet(Transform playerTransform, ValueHandler petMood, float leashLength) 
    {
        //automatically lower mood
        petMood.ValueSubtract(0.25f * Time.deltaTime);
        if (leash.leashIntact == true)
        {
            FindState(playerTransform, petMood);
        } else if (leash.leashIntact == false)
        {
            behaviorState = state.drag;
        }
        DecideMovementTarget(playerTransform, petMood, leashLength);
        
        //MOVE
        transform.position = Vector3.MoveTowards(transform.position, moveTarget, speed * Time.deltaTime);

        
        //ANIMATE
        Animator a = GetComponent<Animator>();
        Vector2 toTarget = moveTarget - transform.position;

        a.SetFloat("moveSpeed", toTarget.magnitude);

        if (Mathf.Abs(toTarget.x) > Mathf.Abs(toTarget.y))
        {
            a.SetBool("moveUD", false);
            a.SetBool("moveLR", true);
            a.SetFloat("moveVelocityLR",toTarget.x);
        }
        else
        {
            a.SetBool("moveUD", true);
            a.SetBool("moveLR", false);
            a.SetFloat("moveVelocityUD", toTarget.y);
        }
            


    }

    private void FindState(Transform player, ValueHandler mood) 
    {
        //FIND BEHAVIOR STATE

        if (transform.position.x > 100 || stuck)
        {
            //stuck locks the pet into its state after reaching the defined finish.
            stuck = true;
            behaviorState = state.finalDrag;
        }

        else if (player.GetComponent<PlayerMovement>().kibble)
        {
            behaviorState = state.kibble;
        }

        else if (mood.value > 2)
        {
            behaviorState = state.follow;
        }

        else if (mood.value > 1)
        {
            behaviorState = state.passive;
        }

        else
        {
            behaviorState = state.drag;
        }


    }

    private void DecideMovementTarget(Transform player, ValueHandler mood, float leashLength) 
    {

        //DECIDE MOVEMENT TARGET

        if (behaviorState == state.finalDrag)
        {
            moveTarget = mugger.position;
            speed = finalDragSpeed;
        }

        else if (behaviorState == state.follow)
        {
            moveTarget = player.position + ((transform.position - player.position).normalized * (leashLength / 2f));
            speed = followSpeed;
        }

        else if (behaviorState == state.kibble)
        {
            moveTarget = player.position + ((transform.position - player.position).normalized * (leashLength / 2f));
            speed = kibbleSpeed;
            mood.ValueSet(3);
        }

        else if (behaviorState == state.passive)
        {
            moveTarget = player.position + ((transform.position - player.position).normalized * (leashLength / 2f));
            speed = passiveSpeed;
        }

        else if (behaviorState == state.drag)
        {
            if (SniffTarget())
            {
                moveTarget = SniffTarget().position;
                speed = dragSpeed;

            }
            else
            {
                if (leash.leashIntact == true)
                {
                    moveTarget = player.position + ((transform.position - player.position).normalized * (leashLength / 2f));
                    speed = passiveSpeed;
                }
                else
                {
                    if (targetAcquired == false)
                    {
                        moveTarget = transform.position + new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), 0);
                        unleashedTarget = moveTarget;
                        speed = passiveSpeed;
                        targetAcquired = true;
                    }
                    else
                    {
                        if (transform.position == unleashedTarget)
                        {
                            targetAcquired = false;
                        }
                        else
                        {
                            moveTarget = unleashedTarget;
                        }
                    }

                }
            }
        }
    }

    bool whimpered = false;
    Transform SniffTarget()
    {
        if (Physics2D.CircleCastAll(transform.position, sniffRadius, Vector3.zero,0,lure).Length > 0)
        {
            RaycastHit2D[] radiusHits = Physics2D.CircleCastAll(transform.position, sniffRadius, Vector3.zero,0,lure);
            Transform chosenTarget = radiusHits[0].transform;
            for(int i = 1; i < radiusHits.Length; i++)
            {
                if (radiusHits[i].collider.gameObject.layer == lure)
                if (Vector3.Distance(radiusHits[i].transform.position,transform.position) < Vector3.Distance(chosenTarget.position, transform.position))
                {
                    chosenTarget = radiusHits[i].transform;
                }
            }

            if (whimpered == false)
            {
                whimpered = true;
                whimper.start();
            }

            return chosenTarget;
        }

        else
        {

            whimpered = false;
            return null;
        }
    }
}
