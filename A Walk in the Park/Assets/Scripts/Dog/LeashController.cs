﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LeashEvent : UnityEngine.Events.UnityEvent { }

public class LeashController : MonoBehaviour
{

    LineRenderer lr;
    [Header("Leash")]
    public float leashLength;
    float leashDurability;
    public float leashDecayRate;
    public float leashMaxDurability;
    public float leashMaxWidth;
    public AnimationCurve leashWidthDecay;
    public bool leashIntact;

    bool leashSnapped = false;
    public bool leashTaut = false;

    [SerializeField]
    LeashEvent leashTautEvent;

    [SerializeField]
    LeashEvent leashSnapEvent;

    //Declares win state
    private VictoryManager victoryManager;

    //The loss message
    [SerializeField] string lossMessage;
    // Start is called before the first frame update
    void Start()
    {
        #region GetComponents
        lr = GetComponent<LineRenderer>();
        #endregion
        victoryManager = FindObjectOfType<VictoryManager>();
        leashDurability = leashMaxDurability;
        leashIntact = true;
    }

    public void RenderLeash(Transform playerTransform) 
    {
        //RENDER LEASH
        lr.SetPositions(new Vector3[] { transform.position, playerTransform.position });
        lr.widthMultiplier = leashMaxWidth * leashWidthDecay.Evaluate(leashDurability / leashMaxDurability);
    }

    public void LeashConstrain(Transform playerTransform) 
    {
        //constrain pet & playerTransform to leash
        //this code is solely responsible for the leash connection between the pet and playerTransform.
        //it has surprisingly fluid movement despite its simplicity.

        if (leashIntact == true)
        {
            if (Vector3.Distance(transform.position, playerTransform.position) > leashLength)
            {

                if (leashTaut == false)
                {
                    leashTaut = true;
                    leashTautEvent.Invoke();
                }

                //leash loses durability
                leashDurability -= leashDecayRate * Time.deltaTime;

                Vector3 point = Vector3.Lerp(transform.position, playerTransform.position, 0.5f);
                playerTransform.position = point + ((playerTransform.position - point).normalized * (leashLength / 2f));
                transform.position = point + ((transform.position - point).normalized * (leashLength / 2f));
            }
            else
            {
                if (Vector3.Distance(transform.position, playerTransform.position) < leashLength * 0.9f)
                {
                    leashTaut = false;
                }
            }
        }

    }

    public float GetLength() 
    {
        return leashLength;
    
    }

    public void Update()
    {
        if (leashDurability < 0)
        {
            leashIntact = false;
            victoryManager.SetLossCondition(lossMessage);
            if (leashSnapped == false)
            {
                leashSnapEvent.Invoke();
                leashSnapped = true;
            }
        }
    }
}
