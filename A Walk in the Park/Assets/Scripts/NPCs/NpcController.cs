﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
public class NpcController : NpcDialogue
{

    [System.Serializable]
    public class Conversation
    {
        public string[] dialogue;
        public Sprite[] expressions;
    }
    [SerializeField]    Conversation[] conversations;
    int conversationsIndex;

    [SerializeField] Conversation[] conversationsPet;
    int conversationsPetIndex;

    [SerializeField] Conversation[] conversationsLocked;
    int conversationsLockedIndex;
    [SerializeField] string conversationKey;

    [SerializeField] bool NPCRelocates;
    [SerializeField] Transform RelocationPosition;

    SpriteRenderer sr;

    new public string name;

    FMOD.Studio.EventInstance happySound;
    FMOD.Studio.EventInstance sadSound;


    //declares a win state
    private VictoryManager victoryManager;

    //Some npcs don't have a win condition, allows for choice
    [SerializeField] bool npcHasWin = false;

    //The win condition for the end of the game
    [SerializeField] string winMessage;

    

    // Start is called before the first frame update
    new void Start()
    {

        base.Start();
        victoryManager = FindObjectOfType<VictoryManager>();
        sr = GetComponent<SpriteRenderer>();

        
        happySound = RuntimeManager.CreateInstance("event:/Voice/" + name + " Happy");
        sadSound = RuntimeManager.CreateInstance("event:/Voice/" + name + " Sad");
    }


    public override void PlayerTouch(PlayerController player)
    {
        //Human Conversations

        if (conversationKey == null || !Inventory.instance.inventory.ContainsKey(conversationKey) )
        {
            sadSound.start();

            if (conversations.Length > 0)
            {
                dialogue = conversations[conversationsIndex].dialogue;
                expressions = conversations[conversationsIndex].expressions;
                Speak();
                if (conversations.Length - 1 > conversationsIndex)
                {
                    conversationsIndex++;
                    if (conversationsIndex >= conversations.Length - 1)
                    {
                        Relocate();
                    }

                }
            }
        }

        else
        {
            if (conversationsLocked.Length > 0)
            {
                happySound.start();

                dialogue = conversationsLocked[conversationsLockedIndex].dialogue;
                expressions = conversationsLocked[conversationsLockedIndex].expressions;
                Speak();
                if (conversationsLocked.Length - 1 > conversationsLockedIndex)
                {
                    conversationsLockedIndex++;
                }

                //check if thequest has been completed
                if (conversationsLocked.Length - 1 == conversationsIndex && npcHasWin == true)
                {
                    victoryManager.SetWinCondition(winMessage);
                }
            }
        }
    }

    public override void PetTouch(PetController pet)
    {
        //Pet Conversations
        if (conversationsPet.Length > 0)
        {
            happySound.start();

            dialogue = conversationsPet[conversationsPetIndex].dialogue;
            expressions = conversationsPet[conversationsPetIndex].expressions;
            Speak();
            if (conversationsPet.Length - 1 > conversationsPetIndex)
            {
                conversationsPetIndex++;
            }
        }
    }

    void Relocate() 
    {
        if (NPCRelocates == true)
        {
            transform.position = RelocationPosition.position;
        }
        
    
    }
}