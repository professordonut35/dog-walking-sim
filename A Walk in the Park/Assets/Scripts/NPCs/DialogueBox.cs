﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour
{
    
    Vector3 initialPos;

    // Start is called before the first frame update
    void Start()
    {
        
        initialPos = Camera.main.ScreenToWorldPoint(transform.position);
        
    }

    private void Update()
    {
        transform.position = Camera.main.WorldToScreenPoint(initialPos);
    }

    

}
