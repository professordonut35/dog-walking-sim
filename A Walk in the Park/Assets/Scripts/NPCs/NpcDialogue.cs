﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NpcDialogue : Interactive
{
    public string[] dialogue;
    public Sprite[] expressions;
    [SerializeField] DialogueBox Textbox;
    [SerializeField] Vector2 positionOffset;
    Transform canvas;
    Text text;
    SpriteRenderer sr;
    DialogueBox dbInstance;

    int dialogueProgress;


    public void Start()
    {
        canvas = GameObject.Find("Canvas").transform;
        sr = GetComponent<SpriteRenderer>();

    }

    public void Speak()
    {
        dialogueProgress = 0;
        if (dbInstance)
        {
            StopAllCoroutines();
            Destroy(dbInstance.gameObject);
        }

        dbInstance = Instantiate(Textbox,canvas);
        text = dbInstance.GetComponentInChildren<Text>();
        dbInstance.transform.position = Camera.main.WorldToScreenPoint( transform.position + (Vector3)positionOffset);
        StartCoroutine(UpdateCharacter());
    }


    IEnumerator UpdateCharacter()
    {
        //set text
        text.text = dialogue[dialogueProgress];

        //change expression
        if (expressions.Length - 1 >= dialogueProgress)
        {
            if(expressions[dialogueProgress] != null)
            {
                sr.sprite = expressions[dialogueProgress];
            }
        }


        //wait duration
        yield return new WaitForSeconds(2);
        //if there is another line, mark progress and restart the cycle
        dialogueProgress++;

        if (dialogue.Length - 1 < dialogueProgress)
        {
            Destroy(dbInstance.gameObject);
        }
        else
        {
            StartCoroutine(UpdateCharacter());
        }
    }
}
