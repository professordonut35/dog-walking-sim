﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryEvent : MonoBehaviour
{
    public Sprite interacted;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Pet")
        {
            GetComponent<SpriteRenderer>().sprite = interacted;
        }
    }
}
