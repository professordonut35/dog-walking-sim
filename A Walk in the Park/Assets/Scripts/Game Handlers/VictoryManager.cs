﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryManager : MonoBehaviour
{
    private LevelManager levelManager;

    private string endScreenText;

    [SerializeField]
    private string endSceneName;

    [SerializeField]
    float waitTime;

    

    void Start()
    {
        VictoryManager[] vicManagers = GameObject.FindObjectsOfType<VictoryManager>();
        if (vicManagers.Length>1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Awake()
    {
        levelManager = GameObject.FindObjectOfType<LevelManager>();
    }

    public void SetLossCondition(string loseCondition) 
    {
        endScreenText =
        "Oh no! \n" +
        loseCondition + "\n" +
        "You didn't complete the level :(";
        Invoke("MoveToEnd", waitTime);
    }

    public void SetWinCondition(string winCondition)
    {
        endScreenText =
        "Well done! \n" +
        winCondition + "\n" +
        "You completed the level :)";
        Invoke("MoveToEnd", waitTime);
    }

    public string getEndState() 
    {
        return endScreenText;
    }

    private void MoveToEnd() 
    {
        levelManager.ChangeScene(endSceneName);
    }




}
