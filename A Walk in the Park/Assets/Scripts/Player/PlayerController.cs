﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Declare Variables
    private PlayerMovement pm;

    #endregion



    // Start is called before the first frame update
    void Start()
    {
        #region Fetch Scripts
        pm = GetComponent<PlayerMovement>();
        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        pm.TugPet();
        pm.UseKibble();
        pm.GetInput();
    }

    private void FixedUpdate()
    {
        pm.MovePlayer();
    }
}
