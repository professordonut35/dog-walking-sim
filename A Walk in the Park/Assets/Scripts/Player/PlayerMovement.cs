﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class PlayerMovement : MonoBehaviour
{

    #region VarDeclaration

    [Header("References")]

    public static PlayerMovement instance;
    public ValueHandler kibbleV;



    //Input

    float horizontal;
    float vertical;
    Vector2 movementInput;
    Vector2 movement;
    Vector2 acceleration;
    Vector2 moveVelocity;
    



    [Header("Speed")]

    float initialAccel;
    public float maxSpeed;
    public float accelRate;
    Rigidbody2D rb;

    //bool moving;

    [Header("Pet Control")]

    public bool kibble;
    public bool tugging;


    #endregion

    FMOD.Studio.EventInstance crunch;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        #region Get Components
        rb = GetComponent<Rigidbody2D>();
        #endregion

        initialAccel = accelRate;
        crunch = RuntimeManager.CreateInstance("event:/Foley/Dog Biscuit Crunch");

    }
    
    public void GetInput()
    {


        //MOVEMENT INPUT

        //get normalized movement input
        //if it weren't normalized, diagonal movement would be twice as fast as cardinal directions
        #region Old Movement Code
        vertical = Input.GetAxisRaw("Vertical");
        horizontal = Input.GetAxisRaw("Horizontal");
        movementInput = new Vector3(horizontal, vertical, 0).normalized;
        
        #endregion


        //movementInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));  
    }

    IEnumerator Unkibble()
    {
        yield return new WaitForSeconds(2);
        kibble = false;

    }

    public void TugPet() 
    {
        //PET CONTROL

        //tug pet
        if (Input.GetMouseButton(1))
        {
            accelRate = initialAccel * 3f;
            print("tug");
            tugging = true;
        }
        else
        {
            accelRate = initialAccel;
            tugging = false;
        }

    }

    public void UseKibble() 
    {
        //use kibble
        if (Input.GetMouseButtonDown(0))
        {
            if (kibble == false)
            {
                kibbleV.ValueSubtract(1);
                kibble = true;
                crunch.start();
                StartCoroutine(Unkibble());
            }
        }


    }

    public void MovePlayer() 
    {

        acceleration = movementInput * accelRate;

        //movementInput = movementInput;

        /*
        acceleration = movementInput.normalized * accelRate;

        if (Mathf.Abs(movementInput.x) < 0.01)
        {

            acceleration = new Vector2(accelRate * -1f * Time.deltaTime , acceleration.y);
        }

        if (movementInput.y < 0.01)
        {
            acceleration = new Vector2(accelRate * -1f * Time.deltaTime, acceleration.x);
        }


        moveVelocity += acceleration * Time.deltaTime;
        if (moveVelocity.magnitude > maxSpeed)
        {
            moveVelocity = moveVelocity.normalized * maxSpeed;
        }
        

        transform.position += (Vector3) moveVelocity;

    */

        if (Mathf.Abs(movementInput.x) < 0.05)
        {
            rb.velocity = new Vector2(rb.velocity.x * 0.9f, rb.velocity.y);
        }
        if (Mathf.Abs(movementInput.y) < 0.05)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.9f);
        }

        //put the movement stuff into the rigidbody
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x + acceleration.x * Time.deltaTime,-maxSpeed, maxSpeed), Mathf.Clamp(rb.velocity.y + acceleration.y * Time.deltaTime, -maxSpeed, maxSpeed));

        //ANIMATE
        Animator a = GetComponent<Animator>();
        
        a.SetFloat("moveSpeed", rb.velocity.magnitude);


        if(rb.velocity.magnitude > 0.1f)
        {
            if (Mathf.Abs(rb.velocity.x) > Mathf.Abs(rb.velocity.y))
            {
                a.SetBool("moveUD", false);
                a.SetBool("moveLR", true);
                a.SetFloat("moveVelocityLR", rb.velocity.x);
            }
            else
            {
                a.SetBool("moveUD", true);
                a.SetBool("moveLR", false);
                a.SetFloat("moveVelocityUD", rb.velocity.y);
            }
        }
        else
        {
            a.SetBool("moveUD", false);
            a.SetBool("moveLR", false);
            a.SetFloat("moveVelocityUD", 0f);
            a.SetFloat("moveVelocityLR", 0f);
        }
       

        //rb.MovePosition(rb.position + movement.normalized * maxSpeed * Time.deltaTime);
    }
}