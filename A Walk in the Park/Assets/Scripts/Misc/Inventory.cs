﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public Sprite empty;
    Slot[] slots;
    public static Inventory instance;

    public Dictionary<string, Sprite> inventory;

    // Start is called before the first frame update
    void Start()
    {
        slots = new Slot[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            slots[i] = transform.GetChild(i).GetComponent<Slot>();
        }

        inventory = new Dictionary<string, Sprite>();

        instance = this;
        
    }

    public void ObtainItem(string name, Sprite picture)
    {
        inventory.Add(name, picture);
        
        for (int i = 0; i < slots.Length; i++)
        {
            if(slots[i].text.text == "empty")
            {
                slots[i].image.sprite = picture;
                slots[i].text.text = name;
                i = slots.Length + 1;
            }
        }
    }

    public void RemoveItem(string name)
    {
        inventory.Remove(name);

        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].text.text == name)
            {
                slots[i].image.sprite = empty;
                slots[i].text.text = "empty";
                i = slots.Length + 1;
            }
        }
    }

}