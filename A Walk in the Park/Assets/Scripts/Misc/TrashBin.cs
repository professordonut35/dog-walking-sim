﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashBin : Interactive
{
    public override void PlayerTouch(PlayerController player)
    {
        for(int i =0; i< Inventory.instance.inventory.Count; i++)
        {
            //if(Inventory.instance.inventory.ContainsKey("Fish Skeleton"))
            {
                Inventory.instance.RemoveItem("Apple Core");
                Inventory.instance.RemoveItem("Fish Skeleton");
                Inventory.instance.RemoveItem("Crumpled Can");
                Inventory.instance.RemoveItem("Trash Bag");
            }
        }
    }
}
