﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomiseObject : MonoBehaviour
{
   

    [SerializeField]
    Vector3 [] SpwanPosition;
    void Start()
    {
        int NewPos = Random.Range(0, SpwanPosition.Length - 1);
        this.transform.position = SpwanPosition[NewPos];
    }

   
}
