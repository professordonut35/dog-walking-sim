﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Item : Interactive
{
    FMOD.Studio.EventInstance pickup;

    private void Start()
    {
        pickup = RuntimeManager.CreateInstance("event:/Foley/Pick Up Item");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    public override void PlayerTouch(PlayerController player)
    {
        Inventory.instance.ObtainItem(gameObject.name,GetComponent<SpriteRenderer>().sprite);

        pickup.start();

        Destroy(gameObject);
    }

}
