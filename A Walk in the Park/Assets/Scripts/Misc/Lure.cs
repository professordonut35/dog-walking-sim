﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Lure : Interactive
{

    [SerializeField] Vector2 moodAddRange;

    FMOD.Studio.EventInstance dogReward;

    private void Start()
    {
        dogReward = RuntimeManager.CreateInstance("event:/Foley/Dog Reward");
    }

    override public void PetTouch(PetController pet)
    {
        pet.mood.ValueAdd(Random.Range(0.5f, 1.3f));
        dogReward.start();
        Destroy(gameObject);
    }
    
}
