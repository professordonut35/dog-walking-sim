﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactive : MonoBehaviour
{

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Pet")
        {
            
            PetTouch(collision.gameObject.GetComponent<PetController>());
            
        }

        if (collision.gameObject == PlayerMovement.instance.gameObject)
        {
            
            PlayerTouch(collision.gameObject.GetComponent<PlayerController>());
            
        }
    }

    public virtual void PetTouch(PetController pet)
    {

    }

    public virtual void PlayerTouch(PlayerController player)
    {

    }

}
