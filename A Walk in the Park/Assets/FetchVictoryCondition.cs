﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FetchVictoryCondition : MonoBehaviour
{

    private VictoryManager victoryManager;
    private Text endText;
    // Start is called before the first frame update
    void Start()
    {
        victoryManager = FindObjectOfType<VictoryManager>();
        endText = this.gameObject.GetComponent<Text>();
        endText.text = victoryManager.getEndState();
    }

   
}
